document.addEventListener('DOMContentLoaded', function () {
    const btnBackToTop = document.getElementById('btn-back-to-top');

    const handleScroll = () => {
        if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
            btnBackToTop.style.display = 'block';
        } else {
            btnBackToTop.style.display = 'none';
        }
    };

    const backToTop = () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
    };

    const handleButtonClick = () => {
        backToTop();
    };

    btnBackToTop.addEventListener('click', handleButtonClick);

    window.addEventListener('scroll', handleScroll);

    window.addEventListener('beforeunload', function () {
        btnBackToTop.removeEventListener('click', handleButtonClick);
        window.removeEventListener('scroll', handleScroll);
    });
});

let toggleBtn = document.querySelector("#navbar-toggle");
let collapse = document.querySelector("#navbar-collapse");
toggleBtn.onclick = () => {
    collapse.classList.toggle("hidden");
    collapse.classList.toggle("flex");
};